package br.fellows.dbaccess.post;

import org.springframework.web.service.annotation.GetExchange;

import java.util.Collection;

public interface PostRepositoryClient extends PostRepository{

    @Override
    @GetExchange("https://jsonplaceholder.typicode.com/posts")
    Collection<Post> findAll();
}
