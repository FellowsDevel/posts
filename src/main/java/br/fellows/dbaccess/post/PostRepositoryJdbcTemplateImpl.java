package br.fellows.dbaccess.post;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.Collection;

@Component
public class PostRepositoryJdbcTemplateImpl implements PostRepository {

    private final JdbcTemplate jdbcTemplate;

    public PostRepositoryJdbcTemplateImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Collection<Post> findAll() throws SQLException {
        return jdbcTemplate.query("select * from Post", (rs, rowNum) ->
                new Post(
                        rs.getLong("id"),
                        rs.getString("title"),
                        rs.getString("body"),
                        rs.getLong("userID")
                )
        );
    }
}
