package br.fellows.dbaccess.post;

import org.springframework.data.repository.ListCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepositoryCrudRepository extends PostRepository, ListCrudRepository<Post, String> {

}
