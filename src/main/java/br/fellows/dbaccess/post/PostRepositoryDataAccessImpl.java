package br.fellows.dbaccess.post;

import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Component
public class PostRepositoryDataAccessImpl implements PostRepository {

    private final DataSource dataSource;

    public PostRepositoryDataAccessImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Collection<Post> findAll() throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement ps = connection.prepareStatement("select * from Post");
        ResultSet rs = ps.executeQuery();

        List<Post> resp = new ArrayList<>(rs.getFetchSize());
        while (rs.next()) {
            resp.add(new Post(
                    rs.getLong("id"),
                    rs.getString("title"),
                    rs.getString("body"),
                    rs.getLong("userID")
            ));
        }
        return resp;
    }
}
