package br.fellows.dbaccess.post;

import org.springframework.jdbc.core.simple.JdbcClient;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.Collection;

@Component
public class PostRepositoryJdbcClientImpl implements PostRepository {

    private final JdbcClient jdbcClient;

    public PostRepositoryJdbcClientImpl(JdbcClient jdbcClient) {
        this.jdbcClient = jdbcClient;
    }

    @Override
    public Collection<Post> findAll() throws SQLException {
        return jdbcClient.sql("select * from Post")
                .query(Post.class)
                .list();
    }
}
