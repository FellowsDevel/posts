package br.fellows.dbaccess.post;

import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Collection;

@Service
public class PostService {

    private final PostRepository postRepository;

    public PostService(PostRepository postRepository) {
        this.postRepository = postRepository;
    }


    public Collection<Post> findAll() throws SQLException {
        return postRepository.findAll();
    }

}
