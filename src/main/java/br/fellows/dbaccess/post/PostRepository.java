package br.fellows.dbaccess.post;

import java.sql.SQLException;
import java.util.Collection;

public interface PostRepository {
    Collection<Post> findAll() throws SQLException;
}
