package br.fellows.dbaccess;

import br.fellows.dbaccess.post.PostRepository;
import br.fellows.dbaccess.post.PostRepositoryClient;
import br.fellows.dbaccess.post.PostService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.JdkClientHttpRequestFactory;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.support.RestClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

@SpringBootApplication
public class DbAccessApplication {

    public static void main(String[] args) {
        SpringApplication.run(DbAccessApplication.class, args);
    }


    @Bean
    RestClient restClient(RestClient.Builder builder) {
        return builder
                .requestFactory(new JdkClientHttpRequestFactory())
                .build();
    }

    /**
     * Criação da implementação da interface PostRepositoryClient
     * com um cliente Rest padrão
     */
    @Bean
    PostRepositoryClient postRepositoryClient(RestClient http) {
        return HttpServiceProxyFactory
                .builderFor(RestClientAdapter.create(http))
                .build()
                .createClient(PostRepositoryClient.class);
    }

    @Bean
    PostRepository postRepository(PostRepositoryClient repo) {
        return repo;
    }

    @Bean
    CommandLineRunner commandLineRunner(PostService postService) {
        return args -> postService.findAll().forEach(System.out::println);
    }
}
