DROP TABLE IF EXISTS Post;

CREATE TABLE Post (
    id integer NOT NULL,
    title varchar(255) NOT NULL,
    body varchar(4000) NOT NULL,
    userId integer NOT NULL,
    primary key(id)
);

INSERT INTO Post (id, title, body, userId ) VALUES (1, 'Hello World', 'The body text', 1);
INSERT INTO Post (id, title, body, userId ) VALUES (2, 'Hello World 2', 'The body text to user 2', 2);

